
var getUrl = window.location.search.slice(1);
getUrl = getUrl.replace(/=/g, '":"');
getUrl = getUrl.replace(/&/g, '","');
getUrl = '{"' + getUrl + '"}';

var object = JSON.parse(getUrl);

const xhr = new XMLHttpRequest();
xhr.open('GET', 'https://restcountries.com/v3.1/alpha/' + `${object.country}`, true);

xhr.onload = function () {

    if (this.status === 200) {
        let obj = JSON.parse(this.responseText);
        console.log(obj[0]);

        var k = 0;
        var nname;
        var curr;
        var lang = "";

        for (key in obj[0].name.nativeName) {
            k = k + 1;
            nname = obj[0].name.nativeName[key].official;
            if (k === 1) {
                k = 0;
                break;
            }
        }

        for (key in obj[0].currencies) {
            k = k + 1;
            curr = obj[0].currencies[key].name;
            if (k === 1) {
                k = 0;
                break;
            }
        }

        for (key in obj[0].languages) {
            lang = lang + obj[0].languages[key] + ", ";
        }

        lang = lang.slice(0, -2);

        var img = `<img class="card-img-top" src="${obj[0].flags.png}" alt="Card image cap">`;
        $("#img_contain").append(img);

        var heading = `<h1><strong>${obj[0].name.common}</strong></h1>`;
        $("#hd").append(heading);

        var text_item = `<p class="card-text">Native Name: ${nname}</p>
            <p class="card-text">Capital: ${obj[0].capital[0]}</p>
            <p class="card-text">Population: ${obj[0].population}</p>
            <p class="card-text">Region: ${obj[0].region}</p>
            <p class="card-text">Sub-Region: ${obj[0].subregion}</p>
            <p class="card-text">Area: ${obj[0].area}</p>
            <p class="card-text">Country Code: ${obj[0].ccn3}</p>
            <p class="card-text">Languages: ${lang}</p>
            <p class="card-text">Currencies: ${curr}</p>
            <p class="card-text">Timezones: ${obj[0].timezones[0]}</p>`;

        $("#text").append(text_item);

        for (key in obj[0].borders) {

            console.log(obj[0].borders[key]);
            var neigh = obj[0].borders[key];

            const xhr2 = new XMLHttpRequest();
            xhr2.open('GET', 'https://restcountries.com/v3.1/alpha/' + `${neigh}`, true);

            xhr2.onload = function () {

                if (this.status === 200) {
                    let obj = JSON.parse(this.responseText);
                    console.log(obj);

                    var temp = `<div class="col-md-4 col-xs-6" style="padding-top: 15px;">
                        <img class=" card-img-top w-75" src="${obj[0].flags.png}" alt="Card image cap">
                        </div>`;

                    $("#neighbour").append(temp);

                }
                else {
                    console.error("Some error occurred");
                }

            }

            xhr2.send();

        }

    }
    else {
        console.error("Some error occurred");
    }
}

xhr.send();
