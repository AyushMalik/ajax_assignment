


window.onload = function load() {

    function startx() {


        const xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://restcountries.com/v3.1/all', true);

        xhr.onload = function () {

            $("#spin").hide();

            if (this.status === 200) {
                let obj = JSON.parse(this.responseText);
                console.log(obj);
                console.log(obj[1]);



                for (i = 0; i < obj.length; i++) {

                    var crr;
                    for (key in obj[i].currencies) {
                        crr = obj[i].currencies[key].name;
                    }

                    var content = `
            <ul id="myUL">
            <li id="myLI">
            <img id="myImg" src="${obj[i].flags.png}" alt="NO Image found" width="150px" height="130px">
            <div>
            <strong id="myHeading" style="padding-top: 20px;text-transform: uppercase;">${obj[i].name.common}</strong>
            <p>Currency : <span id="myCurrency">${crr}</span></p>
            <p>Date and Time : ${dateTime(obj[i].timezones[0])}</p>
            <button onclick="window.location.href = '${obj[i].maps.googleMaps}'" class="btn btn-primary" type="submit">Show Map</button>
            <button onclick=\"window.location.href='/detail.html?country=${obj[i].cca3}'\" class="btn btn-primary" type="button">Detail</button>
            </div>   
            </li>
            </ul>`;


                    function dateTime(timezone) {
                        let offSet1 = timezone.split('C');
                        let offSet2 = offSet1[1].split(':');
                        let offSet3 = parseInt(offSet2[0]);
                        let offSet4 = parseInt(offSet2[1]);
                        let offSet = offSet3 + offSet4 / 60;
                        let x = new Date();
                        let month = x.getMonth();
                        let months = [
                            'Jan', 'Feb',
                            'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'

                        ];
                        let day = x.getDate();
                        let year = x.getFullYear();
                        let str1 = '';
                        let str2 = '';
                        let str = '';
                        str1 = day + ' ' + months[month] + ' ' + year;
                        let localTime = x.getTime();
                        let localOffset = x.getTimezoneOffset() * 60000;
                        let utc = localTime + localOffset;
                        let nd = new Date(utc + 3600000 * offSet);
                        let h = nd.getHours();
                        let m = nd.getMinutes();
                        let h1 = '';
                        let m1 = '';
                        if (h < 10) {
                            h1 = h1 + '0' + h.toString();
                        }
                        if (m < 10) {
                            m1 = m1 + '0' + m.toString();
                        }
                        if (h < 10) {
                            if (m < 10) {
                                str2 = h1 + ':' + m1;
                            }
                            else {
                                str2 = h1 + ':' + m;
                            }
                        }
                        else {
                            if (m < 10) {
                                str2 = h + ':' + m1;
                            }
                            else {
                                str2 = h + ':' + m;
                            }
                        }
                        str = str1 + ' and ' + str2;
                        return str;
                    }

                    $(".container").append(content);

                }


            }
            else {
                console.error("Some error occured");
            }
        }

        xhr.send();
    }

    let value = "";
    if (value === "")
        startx();

    $('#get-country').on('click', function () {
        value = $('#search-country').val();

        console.log(value);

        $('.container').empty();
        if (value === "")
            startx();



        if (value !== "") {
            const xhr = new XMLHttpRequest();
            xhr.open('GET', 'https://restcountries.com/v3.1/name/' + value, true);

            xhr.onload = function () {

                if (this.status === 200) {
                    let obj = JSON.parse(this.responseText);
                    console.log(obj);



                    var crr;
                    for (key in obj[0].currencies) {
                        crr = obj[0].currencies[key].name;
                    }

                    console.log(obj[0].currencies[key].name);

                    var content = `
                <ul id="myUL">
                <li id="myLI">
                <img id="myImg" src="${obj[0].flags.png}" alt="NO Image found" width="150px" height="130px">
                <div>
                <strong id="myHeading" style="padding-top: 20px;text-transform: uppercase;">${obj[0].name.common}</strong>
                <p>Currency : <span id="myCurrency">${crr}</span></p>
                <p>Date and Time : ${dateTime(obj[0].timezones[0])}</p>
                <button onclick="window.location.href = '${obj[0].maps.googleMaps}'" class="btn btn-primary" type="submit">Show Map</button>
                <button onclick=\"window.location.href='/detail.html?country=${obj[0].cca3}'\" class="btn btn-primary" type="button">Detail</button>
                </div>   
                </li>
                </ul>`;


                    function dateTime(timezone) {
                        let offSet1 = timezone.split('C');
                        let offSet2 = offSet1[1].split(':');
                        let offSet3 = parseInt(offSet2[0]);
                        let offSet4 = parseInt(offSet2[1]);
                        let offSet = offSet3 + offSet4 / 60;
                        let x = new Date();
                        let month = x.getMonth();
                        let months = [
                            'Jan', 'Feb',
                            'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'

                        ];
                        let day = x.getDate();
                        let year = x.getFullYear();
                        let str1 = '';
                        let str2 = '';
                        let str = '';
                        str1 = day + ' ' + months[month] + ' ' + year;
                        let localTime = x.getTime();
                        let localOffset = x.getTimezoneOffset() * 60000;
                        let utc = localTime + localOffset;
                        let nd = new Date(utc + 3600000 * offSet);
                        let h = nd.getHours();
                        let m = nd.getMinutes();
                        let h1 = '';
                        let m1 = '';
                        if (h < 10) {
                            h1 = h1 + '0' + h.toString();
                        }
                        if (m < 10) {
                            m1 = m1 + '0' + m.toString();
                        }
                        if (h < 10) {
                            if (m < 10) {
                                str2 = h1 + ':' + m1;
                            }
                            else {
                                str2 = h1 + ':' + m;
                            }
                        }
                        else {
                            if (m < 10) {
                                str2 = h + ':' + m1;
                            }
                            else {
                                str2 = h + ':' + m;
                            }
                        }
                        str = str1 + ' and ' + str2;
                        return str;

                    }
                    $(".container").append(content);

                }
                else {
                    console.error("Some error occured");
                }
            }

            xhr.send();

        }

    })

}
